function log_b = log_mul_Gau (mean_i, var_i, c_j, o_i)
[dim, num_of_mix] = size(mean_i);
log_N = -Inf(1,num_of_mix);
log_c = -Inf(1,num_of_mix);
for m = 1:num_of_mix
    log_N(m) = -1/2*(dim*log(2*pi) + sum(log(var_i(:,m))) + sum((o_i - mean_i(:,m)).*(o_i - mean_i(:,m))./var_i(:,m)));
    log_c(m) = log(c_j(m));
end
y = -Inf(1,num_of_mix);
ymax = -Inf;
for m = 1:num_of_mix
    y(m) = log_N(m) + log_c(m);
    if y(m) > ymax
        ymax = y(m);
    end
end
if ymax == Inf
    log_b = Inf;
else
    sum_exp = 0;
    for m = 1:num_of_mix
        if ymax == -Inf && y(m) == -Inf
            sum_exp = sum_exp + 1;
        else
            sum_exp = sum_exp + exp(y(m) - ymax);
        end
    end
    log_b = ymax + log(sum_exp);
end
end
