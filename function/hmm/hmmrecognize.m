function digit = hmmrecognize(hmmname)
if nargin == 0
    hmmname = 'models\HMM.mat';
end

% 提取mfcc特征生成temp.mfc文件
dr_wav2mfcc_e_d_a;

%读取temp.mfc中的特征参数
filename = 'function\temp.mfc';
mfcfile = fopen(filename, 'r', 'b' );
nSamples = fread(mfcfile, 1, 'int32');
sampPeriod = fread(mfcfile, 1, 'int32')*1E-7;
sampSize = fread(mfcfile, 1, 'int16');
dim = 0.25*sampSize; % dim = 39
parmKind = fread(mfcfile, 1, 'int16');
features = fread(mfcfile, [dim, nSamples], 'float');
fclose(mfcfile);

%识别
load(hmmname);
[~, ~, ~, num_of_model] = size(HMM.mean);
fopt_max = -Inf; digit = -1;
file = fopen('function\hmm_result.txt','w');
for p = 1:num_of_model
    fopt = GMM_HMM_viterbi(HMM.mean(:,:,:,p), HMM.var(:,:,:,p), HMM.weight(:,:,p), HMM.Aij(:,:,p), features);
    fprintf(file,'%d\t%.2f\n',p,fopt);
    if fopt > fopt_max
        digit = p;
        fopt_max = fopt;
    end
end
fclose(file);