# DTW

#### 介绍

    文件夹内含DTW算法相关函数

#### 内容

- compare.m : 主函数，程序调用进行比较
- enframe.m : 用于输入信号的分帧
- filter1.m : 滤波器，过滤输入信号的噪音
- fm.mat : 存储Zhang Ding同学的录音
- MFCC_extract.m : 输入信号的MFCC参数提取
- process_vad.m : 输入信号头尾静音切割
- strcat.m : 字符串连接函数（以防MATLAB没有）
