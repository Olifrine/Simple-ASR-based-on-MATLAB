function [kind,dis] = compare(input)
%将输入的语音与12个模板语音比较，返回的kind是四类模板文件的第几类
    load 'fm.mat';%加载模板音频文件特征
%     [input,FS]=audioread(filename);%读取输入音频
    x1=filter(filter1,input);%滤波
    y1=x1./max(x1);
    fm_input=MFCC_extract(y1);%vad静默处理+提取MFCC
    fm_input(1,:)=[];%去掉第一行（第一行无效数据该去掉）
    
    kind=0;%初始化
    sum=0;%初始化
    min_ave=99999999;%初始化
    %将输入的语音与12个模板语音比较
    for i=1:length(yy)
        dis(i)=dtw(yy(i).mfcc,fm_input);%计算输入音频特征与第i个模板音频特征的距离
        sum=sum+dis(i);
        if mod(i,3)==0%每计算完与三个模板文件（同一类模板文件）的距离
            ave=sum/3;%就计算与这一类文件距离的平均值
            sum=0;%更新距离之和为0
            if ave<min_ave%若发现更相似的音频
                min_ave=ave;%更新平均值最小值
                kind=i/3;%记录类别
            end
        end
    end
end