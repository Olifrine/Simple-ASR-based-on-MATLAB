function fm=MFCC_extract(y)
%读取音频文件的第t1-t2s的特征
fs = 24000;
%% VAD做静默处理
%得到处理后的音频y
y=process_vad(y);
%% MFSC
%对音频信号进行分帧
y0=buffer(y,256); %将矩阵等分为256行，最后一列不够的补0；(256为帧移）
[h,L]=size(y0);%需要知道y0有多少列
L=L-1;%写成矩阵后比y0少一列，所以去掉最后一列
y1=zeros(512,L);%（512=256*2=每一帧有512个采样点）
for j=1:L
        y1(:,j)=[y0(:,j);y0(:,j+1)];
end
%分帧音频数据幅度-采样点的波形
%加汉窗
n=0:511;
wn=0.5*(1-cos(2*pi*n/511));
%加窗
y2=zeros(512,L);
for j=1:L
    for i=1:512
    y2(i,j)=wn(i)*y1(i,j);
    end
end
%加窗后的波形
%离散傅里叶变换+求模
f_abs=zeros(512,L);
for j=1:L
    f_abs(:,j)=abs(fft(y2(:,j)));
end
%% 源码这里只取了前257个点，后来发现是512\2+1得来
f=zeros(257,L);
f=f_abs(1:257,:);
nf=0:256;
%梅尔滤波器组
fs=16000;
fl=0; fh=fs/2;
bl=1125*log(1+fl/700);%将频率转换为Mel频率
bh=1125*log(1+fh/700);
%% 滤波器个数，原值是64
p=64;%滤波器个数
nfft=512;%FFT点数
B=bh-bl;
y_mel=linspace(0,B,p+2);%产生0到B之间p+2个数
Fb=700*(exp(y_mel/1125)-1);%将Mel频率转换为频率
W2=nfft/2+1;%fs/2内对应的FFT点数
df=fs/nfft;
freq=(0:W2-1)*df;%采样频率值
bank=zeros(64,W2);%生成一个64行W2列的全零数组
for k=2:p+1%why从2开始？因为k-1
    f1=Fb(k-1); 
    f2=Fb(k+1); 
    f0=Fb(k);
    n1=floor(f1/df)+1;%f(m-1)在频域中的谱线索引号
    n2=floor(f2/df)+1;%f(m+1)在频域中的谱线索引号
    n0=floor(f0/df)+1;%f(m)在频域中的谱线索引号。f(m)是从0开始，而在MATLAB中数组的索引是从1开始，所以要加1，否则会出现index=0的错误
    for i=1 : W2
        if i>=n1 & i<=n0
            bank(k-1,i)=(i-n1)/(n0-n1);
        elseif i>n0 & i<=n2
            bank(k-1,i)=(n2-i)/(n2-n0);
        end
    end
end

fm=zeros(64,L);
fm=log(bank*f);
